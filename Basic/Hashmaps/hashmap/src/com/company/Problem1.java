package com.company;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        String first,second;
        System.out.print("Enter first string: ");
        first=scanner.next();
        System.out.print("Enter second string: ");
        second=scanner.next();
        boolean check=containSubstring(first,second);
        if(check){
            System.out.println("Two string share a common sub-string");
        }else{
            System.out.println("There is no common sub-string between given strings");
        }
    }

    private static boolean containSubstring(String first, String second) {
        boolean check=false;
        for(int i=0;i<first.length()-1;i++){
            String subString=first.substring(i,i+2);
            check=second.contains(subString);
            if(check){
//                    System.out.println("Sub string is: "+subString);
                break;
            }
        }
        return check;
    }
}
