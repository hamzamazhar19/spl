package com.company;

import java.util.Scanner;

public class Problem2 {

    public static void main(String[] args) {
        int max;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of array : ");
        int length = scanner.nextInt();
        int[] in = new int[length];
        for(int i=0;i<length;i++){
            System.out.print("Enter array element "+(i+1)+" : ");
            in[i]=scanner.nextInt();
        }
        max=in[0];
        for(int i=0;i< in.length;i++){
            if(in[i]>max)
                max=in[i];
        }
        System.out.println("Max : "+max);
    }

}
