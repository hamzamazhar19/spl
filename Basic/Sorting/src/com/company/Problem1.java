package com.company;

import java.util.Scanner;

public class Problem1 {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of array : ");
        int length = scanner.nextInt();
        int[] in = new int[length];
        for(int i=0;i<length;i++){
            System.out.print("Enter array element "+(i+1)+" : ");
            in[i]=scanner.nextInt();
        }
        System.out.println("Note: pivot should be a unique value and must exist in the given array !!");
        System.out.print("Enter pivot value : ");
        int pivot = scanner.nextInt();

        int[] out = distributeArray(in,pivot);
        for(int i=0;i< out.length;i++){
            System.out.println(out[i]);
        }
    }

    private static int[] distributeArray(int[] in, int pivot) {
        int[] out=new int[in.length];
        int min_length=0;
        int max_length=0;

        for(int i=0;i< in.length;i++){
            if(in[i]>pivot)
                max_length++;
            else if(in[i]<pivot)
                min_length++;

        }
        System.out.println("min="+min_length+"////max="+max_length);
        int[] min = new int[min_length];
        int[] max = new int[max_length];
        int index=0;
        int index_min=0;
        for(int i=0;i< in.length;i++){
            if(in[i]>pivot){
                max[index]=in[i];
                index++;
            } else if(in[i]<pivot){
                min[index_min]=in[i];
                index_min++;
            }
        }

        int[]min_sorted=sortArray(min);
        int[]max_sorted=sortArray(max);

        for(int i=0;i<min_length;i++)
            out[i]=min_sorted[i];
        out[min_length]=pivot;
        for(int i=1;i<=max_length;i++){
            out[min_length+i]=max_sorted[i-1];
        }


        return out;
    }
    private static int[] sortArray(int[] in){
        int temp;
        for (int i = 0; i < in.length; i++) {
            for (int j = i+1; j < in.length; j++) {
                if(in[i] > in[j]) {
                    temp = in[i];
                    in[i] = in[j];
                    in[j] = temp;
                }
            }
        }
        return in;
    }
}
