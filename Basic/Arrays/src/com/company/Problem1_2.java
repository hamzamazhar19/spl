package com.company;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Problem1_2 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);



        // write your code here
      System.out.print("Enter the length of array : ");
      int length = scanner.nextInt();
      System.out.println("array length is :" + length);
      int[] in = new int[length];
      for(int i=0;i<length;i++){
          System.out.print("Enter array element "+(i+1)+" : ");
          in[i]=scanner.nextInt();
      }
        System.out.println("Enter 1 for rotating array to the left !");
        System.out.println("Enter 2 for array reversal !");
        System.out.println("Enter 3 for exit !");
      int i=0;
      while (i!=3){
          System.out.print("Enter your choice : ");
          i=scanner.nextInt();
          if(i==1){
              System.out.print("Enter number of rotations : ");
              int rotations = scanner.nextInt();
              int[] out = leftRotate(in,rotations);
              displayArray(out);
          }else if(i==2){
              int[] out = reverse(in,in.length-1);
              displayArray(out);
          }else if(i!=3){
              System.out.print("Enter a valid choice ! ");
          }
      }
    }

    private static int[] leftRotate(int[] arr, int n) {
        for(int j=0 ;j<n;j++){
            int i, temp;
            temp = arr[0];
            for (i = 0; i < arr.length - 1; i++)
                arr[i] = arr[i + 1];
            arr[arr.length-1] = temp;
        }
        return arr;
    }

    public static void displayArray(int[] out){
        for(int j=0;j<out.length;j++)
            System.out.println("Rotated array element "+(j+1)+" : "+out[j]);
    }

    public static int[] reverse(int[] arr, int n) {
        for(int j=0 ;j<n;j++){
            int i, temp;
            temp = arr[0];
            for (i = 0; i < arr.length - 1-j; i++)
                arr[i] = arr[i + 1];
            arr[arr.length-j-1] = temp;
        }
        return arr;
    }

}
