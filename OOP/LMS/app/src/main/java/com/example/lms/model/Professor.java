package com.example.lms.model;

import java.io.Serializable;
import java.util.List;

public class Professor extends User implements Serializable {
    private String designation, qualification;

    public Professor(int id, String firstName, String lastName, String password, String userName, int age, String designation, String qualification, List<Courses> courses) {
        super(firstName, lastName, password, userName, age , id,courses);
        this.designation = designation;
        this.qualification = qualification;
    }

    public Professor() {
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Override
    public String getProfile() {
        return getFirstName()+" "+getLastName()+" is "+getDesignation()+" at LUMS";
    }
}
