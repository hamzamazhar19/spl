package com.example.lms.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.lms.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_activity);
    }
}