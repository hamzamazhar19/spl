package com.example.lms.utils;

import com.example.lms.model.Courses;
import com.example.lms.model.Professor;
import com.example.lms.model.Student;

import java.util.ArrayList;
import java.util.List;

public class Singleton {
    private static Singleton single_instance = null;

    // variable of type String
    public List<Professor> professorsList;
    public List<Student> studentsList;
    public List<Courses> coursesList;

    // private constructor restricted to this class itself
    private Singleton()
    {
        setProfessors();
        setStudents();
        setCourses();
    }

    private void setCourses() {
        coursesList = new ArrayList<>();
        coursesList.add(new Courses(15, "Intro. to Programming", "CSI-101", "LUMS", 50));
        coursesList.add(new Courses(14, "Intro. to Computing", "CSI-201", "LUMS", 50));
        coursesList.add(new Courses(12, "OOP", "CSI-121", "LUMS", 50));
        coursesList.add(new Courses(13, "Software Engineering", "CSI-301", "LUMS", 50));
        coursesList.add(new Courses(13, "Software Engineering II", "CSI-302", "LUMS", 50));
        coursesList.add(new Courses(12, "Mobile Application", "CSI-401", "LUMS", 50));

    }
    
    private void setStudents() {
        studentsList = new ArrayList<>();
        List<Courses> courses = new ArrayList<>();
        courses.add(new Courses(12, "OOP", "CSI-121", "LUMS", 50));
        courses.add(new Courses(12, "Mobile Application", "CSI-401", "LUMS", 50));
        studentsList.add(new Student(35,"ATIF", "SHAHZAD", "12345", "ATIF", 22, 888, "senior",courses));
        studentsList.add(new Student(58,"AYAAN", "ZOHAIB", "123456", "AYAAN", 21, 889, "senior",courses));
        courses=new ArrayList<>();
        courses.add(new Courses(13, "Software Engineering", "CSI-301", "LUMS", 50));
        courses.add(new Courses(14, "Intro. to Computing", "CSI-201", "LUMS", 50));
        studentsList.add(new Student(36,"RAYAN", "HARIS", "123457", "RAYAN", 20, 896, "junior",courses));
        studentsList.add(new Student(88,"RASHID", "ALI", "123458", "RASHID", 20, 456, "junior",courses));
        courses = new ArrayList<>();
        courses.add(new Courses(15, "Intro. to Programming", "CSI-101", "LUMS", 50));
        courses.add(new Courses(13, "Software Engineering", "CSI-301", "LUMS", 50));
        studentsList.add(new Student(22,"SUFIAN", "KASHIF", "123459", "SUFIAN", 19, 811, "freshman",courses));
    }

    private void setProfessors() {
        professorsList = new ArrayList<>();
        List<Courses> courses = new ArrayList<>();
        courses.add(new Courses(12, "OOP", "CSI-121", "LUMS", 50));
        courses.add(new Courses(12, "Mobile Application", "CSI-401", "LUMS", 50));
        professorsList.add(new Professor(12,"Abdullah", "Ali", "12345", "Prof. Abdullah Ali", 39, "Assistant Prof.", "MSCS",courses));
        courses = new ArrayList<>();
        courses.add(new Courses(14, "Intro. to Computing", "CSI-201", "LUMS", 50));
        professorsList.add(new Professor(14,"Ahsan", "Ali", "123456", "Prof. Ahsan", 49, "Senior Prof.", "MSCS",courses));
        courses = new ArrayList<>();
        courses.add(new Courses(15, "Intro. to Programming", "CSI-101", "LUMS", 50));
        professorsList.add(new Professor(15,"Mujahid", "Naseer", "123457", "Prof. Mujahid", 38, "Assistant Prof.", "BSCS",courses));
        courses=new ArrayList<>();
        courses.add(new Courses(13, "Software Engineering", "CSI-301", "LUMS", 50));
        courses.add(new Courses(13, "Software Engineering II", "CSI-302", "LUMS", 50));
        professorsList.add(new Professor(13,"Moazzam", "Ali", "123458", "Prof. Moazzam", 29, "Lecturer", "PhD",courses));
    }

    // static method to create instance of Singleton class
    public static Singleton getInstance()
    {
        if (single_instance == null)
            single_instance = new Singleton();

        return single_instance;
    }
}
