package com.example.lms;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.example.lms.activities.LoginActivity;
import com.example.lms.model.Professor;
import com.example.lms.utils.Singleton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(() -> {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
//            if(user == null){
//                startActivity(new Intent(SplashActivity.this, ReplacerActivity.class));
//            }else{
//                startActivity(new Intent(SplashActivity.this, ReplacerActivity.class));
//            }

        }, 1500 );
    }
}