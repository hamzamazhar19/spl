package com.example.lms.model;

import java.io.Serializable;
import java.util.List;

public class Student extends User implements Serializable {
    private int roll_number;
    private String status;


    public Student(int id, String firstName, String lastName, String password, String userName, int age, int roll_number, String status, List<Courses> courses) {
        super(firstName, lastName, password, userName, age,id,courses);
        this.roll_number = roll_number;
        this.status = status;

    }

    public Student() {
    }

    public int getRoll_number() {
        return roll_number;
    }

    public void setRoll_number(int roll_number) {
        this.roll_number = roll_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getProfile() {
        return getFirstName()+" "+getLastName()+" is a "+getStatus()+" student at LUMS";
    }
}
