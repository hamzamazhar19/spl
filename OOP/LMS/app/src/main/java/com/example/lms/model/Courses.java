package com.example.lms.model;

import java.io.Serializable;

public class Courses implements Serializable {
    private String courseName,courseCode,offeringSchool;
    private int prof_id,capacity;

    public Courses() {
    }

    public Courses(int prof_id, String courseName, String courseCode, String offeringSchool, int capacity) {
        this.prof_id = prof_id;
        this.courseName = courseName;
        this.courseCode = courseCode;
        this.offeringSchool = offeringSchool;
        this.capacity = capacity;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getOfferingSchool() {
        return offeringSchool;
    }

    public void setOfferingSchool(String offeringSchool) {
        this.offeringSchool = offeringSchool;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
