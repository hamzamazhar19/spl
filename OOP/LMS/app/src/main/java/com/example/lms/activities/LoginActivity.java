package com.example.lms.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.lms.R;
import com.example.lms.model.Professor;
import com.example.lms.model.Student;
import com.example.lms.model.User;
import com.example.lms.utils.Singleton;
import com.google.android.material.button.MaterialButton;

import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    CheckBox isStudent;
    EditText userName,password;
    MaterialButton login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isStudent = findViewById(R.id.checkBox);
        userName = findViewById(R.id.personNameET);
        password = findViewById(R.id.passwordET);
        login = findViewById(R.id.loginBT);

        login.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.equals(login)){
            if(isStudent.isChecked()){
                if(!userName.getText().toString().isEmpty()){
                    if(!password.getText().toString().isEmpty()){
                        Student student = getStudent(userName.getText().toString(), Singleton.getInstance().studentsList);
                        if(student != null){
                            if(student.getPassword().equals(password.getText().toString())){
                                Intent i =new Intent(LoginActivity.this,HomeActivity.class);
                                i.putExtra("student", student);
                                startActivity(i);
                                finish();
                            }else{
                                password.setError("Enter valid password.");
                            }
                        }else{
                            userName.setError("Enter valid user name.");
                        }
                    }else {
                        password.setError("Enter valid password.");
                    }
                }else{
                    userName.setError("Enter valid user name.");
                }
            }else{
                if(!userName.getText().toString().isEmpty()){
                    if(!password.getText().toString().isEmpty()){
                        Professor professor = getProfessor(userName.getText().toString(), Singleton.getInstance().professorsList);
                        if(professor != null){
                            if(professor.getPassword().equals(password.getText().toString())){
                                Intent i =new Intent(LoginActivity.this,HomeActivity.class);
                                i.putExtra("professor", professor);
                                startActivity(i);
                                finish();
                            }else{
                                password.setError("Enter valid password.");
                            }
                        }else{
                            userName.setError("Enter valid user name.");
                        }
                    }else {
                        password.setError("Enter valid password.");
                    }
                }else{
                    userName.setError("Enter valid user name.");
                }
            }
        }
    }

    private Student getStudent(String toString, List<Student> studentsList) {
        Student student = null;
        for(int i=0;i<studentsList.size();i++){
            Student student1 = studentsList.get(i);
            if(student1.getUserName().equals(toString)){
                student = student1;
                return student;
            }
        }
        return  student;
    }

    private Professor getProfessor(String toString, List<Professor> professorsList) {
        Professor professor = null;
        for(int i=0;i<professorsList.size();i++){
            Professor student1 = professorsList.get(i);
            if(student1.getUserName().equals(toString)){
                professor = student1;
                return professor;
            }
        }
        return  professor;
    }
}