package com.company;

import java.util.HashMap;
import java.util.Map;

public class Problem1 {
    private static final Map<Integer, Integer> memo = new HashMap<>();
    public static void main(String[] args) {
	// write your code here
        int out=fib(6);
        System.out.println(out);
    }

    public static int fib(int n) {

        if (n < 0) {
            throw new IllegalArgumentException(
                    "Index was negative. No such thing as a negative index in a series.");
            
            // base cases
        } else if (n == 0 || n == 1) {
            return n;
        }

        // check hashmap
        if (memo.containsKey(n)) {
            System.out.printf("grabbing memo[%d]\n", n);
            return memo.get(n);
        }

        System.out.printf("computing fib(%d)\n", n);
        int result = fib(n - 1) + fib(n - 2);

        // memoize:- memoization basically reduces recursive calls by storing the result of unique calls
        // if the same input comes again we retrieve result from where we stored instead of processing input again
        memo.put(n, result);

        return result;
    }

}
