package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int target;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of array : ");
        int length = scanner.nextInt();
        int[] in = new int[length];
        for(int i=0;i<length;i++){
            System.out.print("Enter array element "+(i+1)+" : ");
            in[i]=scanner.nextInt();
        }
        System.out.print("Enter target sum : ");
        target=scanner.nextInt();
        Pairs[] pairs=getPairsCount(in, target);
        if(pairs.length>0){
            System.out.println("Pairs are : ");
            for (Pairs pair : pairs) System.out.println(pair.getFirst()+","+pair.getSecond());
        }else{
            System.out.println("No pairs found");
        }

    }

    public static Pairs[] getPairsCount(int[] arr, int sum)
    {
        ArrayList<Pairs> arrayList = new ArrayList<>();

        // Consider all possible pairs and check their sums
        for (int i = 0; i < arr.length; i++)
            for (int j = i + 1; j < arr.length; j++)
                if ((arr[i] + arr[j]) == sum){
                    if(arrayList.size()==0){
                        arrayList.add(new Pairs(arr[i],arr[j]));
                    }else{
                        for(int f=0;f<arrayList.size();f++){
                            Pairs pair = arrayList.get(f);
                            if( pair.getFirst() != arr[i] && pair.getFirst() != arr[j] && pair.getSecond() != arr[i] &&pair.getSecond() != arr[j] )
                                arrayList.add(new Pairs(arr[i],arr[j]));
                        }
                    }
                }

        Pairs[] stockArr = new Pairs[arrayList.size()];
        stockArr = arrayList.toArray(stockArr);

        return stockArr;
    }
}
