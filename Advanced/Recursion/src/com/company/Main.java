package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the length of array : ");
        int length = scanner.nextInt();
        int[] in = new int[length];
        for(int i=0;i<length;i++){
            System.out.print("Enter array element "+(i+1)+" : ");
            in[i]=scanner.nextInt();
        }
        System.out.println((int)(calMean(in, length)));
    }
    static float calMean(int[] in, int length)
    {
        if (length == 1)
            return (float)in[length-1];
        else
            return ((calMean(in, length-1)*(length-1) +
                    in[length-1]) / length);
    }
}
